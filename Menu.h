#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Point.h"
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include <vector>
#include <iostream>


class Menu
{
public:

	Menu();
	~Menu();

	void newShape();
	void newCircle();
	void newArrow();
	void newTriangle();
	void newRectangle();
	void modifyMenu();
protected:
	std::vector<Shape*> _shapes;
private:
	Canvas _canvas;
};

