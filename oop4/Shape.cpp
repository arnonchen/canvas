#include "Shape.h"
#include <string>
#include <iostream>
Shape::Shape(const std::string& name, const std::string& type)
{
	_name = name;
	_type = type;
}

Shape:: ~Shape()
{
}

void Shape::printDetails() const
{
	std::cout << _name << "\n" << _type;
}

std::string Shape::getType() const
{
	return _type;
}

std::string Shape::getName() const
{
	return _name;
}