#include "Rectangle.h"

myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name) : Polygon(type, name)
{
	_points.push_back(a);
	_points.push_back(Point(a.getX() + length,a.getY() + width));
}

myShapes::Rectangle::~Rectangle()
{
	_points.clear();
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

double myShapes::Rectangle::getArea() const
{
	return((_points[1].getX() - _points[0].getX()) * (_points[1].getY() - _points[0].getY()));
}

double myShapes::Rectangle::getPerimeter() const
{
	return(2 * (_points[1].getX() - _points[0].getX()) + 2 * (_points[1].getY() - _points[0].getY()));
}

void myShapes::Rectangle::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
}