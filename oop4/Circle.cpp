#include "Circle.h"
#include <math.h>
#include <iostream>
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name) : Shape(name, type), _center(center)
{
	_radius = radius;
}
Circle::~Circle()
{
}

const Point& Circle::getCenter() const
{
	return _center;
}

double Circle::getRadius() const
{
	return _radius;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

double Circle::getArea() const
{
	return (_radius * _radius * PI);
}

double Circle::getPerimeter() const
{
	return (_radius * PI * 2);
}

void Circle::move(const Point& other)
{
	_center += other;
}