#include "Menu.h"

Menu::Menu() 
{
	int choice = 0;
	while (choice != 3)
	{
		std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit.\n";
		std::cin >> choice;
		switch (choice)
		{
		case 0:
			newShape();
			break;
		case 1:
			modifyMenu();
			break;
		case 2:
			for (int i = 0; i < _shapes.size(); i++)
			{
				_shapes[i]->clearDraw(_canvas);
				delete (_shapes[i]);
			}
			_shapes.clear();
			break;
		default:
			break;
		}
	}
}

void Menu::newShape()
{
	int choice = -1;
	while (choice > 3 || choice < 0)
	{
		std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle.\n";
		std::cin >> choice;
		switch (choice)
		{
		case 0:
			newCircle();
			break;
		case 1:
			newArrow();
			break;
		case 2:
			newTriangle();
			break;
		case 3:
			newRectangle();
			break;
		default:
			break;
		}
	}
}


Menu::~Menu()
{
	for (int i = 0; i < _shapes.size(); i++)
	{
		delete (_shapes[i]);
	}
	_shapes.clear();
}

void Menu::newCircle()
{
	double x;
	double y;
	double radius;
	std::string type = "circle";
	std::string name;
	std::cout << "Please enter X:\n";
	std::cin >> x;
	std::cout << "Please enter Y:\n";
	std::cin >> y;
	Point center = Point(x, y);
	std::cout << "Please enter radius :\n";
	std::cin >> radius;
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	Shape* newCircle = new Circle(center, radius, "Circle", name);
	newCircle->draw(_canvas);
	_shapes.push_back(newCircle);
};


void Menu::newArrow()
{
	double x;
	double y;
	std::string type = "arrow";
	std::string name;
	std::cout << "Enter the X of point number: 1\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1\n";
	std::cin >> y;
	Point a = Point(x, y);
	std::cout << "Enter the X of point number: 2\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 2\n";
	std::cin >> y;
	Point b = Point(x, y);
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	Shape* newArrow = new Arrow(a, b, "Arrow", name);
	newArrow->draw(_canvas);
	_shapes.push_back(newArrow);
};

void Menu::newTriangle()
{
	double x;
	double y;
	std::string type = "triangle";
	std::string name;
	std::cout << "Enter the X of point number: 1\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1\n";
	std::cin >> y;
	Point a = Point(x, y);
	std::cout << "Enter the X of point number: 2\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 2\n";
	std::cin >> y;
	Point b = Point(x, y);
	std::cout << "Enter the X of point number: 3\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 3\n";
	std::cin >> y;
	Point c = Point(x, y);
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	Shape* newTriangle = new Triangle(a, b, c, "Triangle", name);
	newTriangle->draw(_canvas);
	_shapes.push_back(newTriangle);
};

void Menu::newRectangle()
{
	double x;
	double y;
	double length;
	double width;
	std::string type = "rectangle";
	std::string name;
	std::cout << "Enter the X of point number: 1\n";
	std::cin >> x;
	std::cout << "Enter the Y of point number: 1\n";
	std::cin >> y;
	Point a = Point(x, y);
	std::cout << "Please enter the length of the shape:\n";
	std::cin >> length;
	std::cout << "Please enter the width of the shape:\n";
	std::cin >> width;
	std::cout << "Please enter the name of the shape:\n";
	std::cin >> name;
	Shape* newRectangle = new myShapes::Rectangle(a, length, width, "Rectangle", name);
	newRectangle->draw(_canvas);
	_shapes.push_back(newRectangle);
};

void Menu::modifyMenu()
{
	bool flag = true;
	int shapeChoice = 0;
	int action = 0;
	int x, y;
	while (flag)
	{
		if (_shapes.size() == 0)
			return;
		for (int i = 0; i < this->_shapes.size(); i++)
		{
			std::cout << "Enter " << i << " for " << this->_shapes[i]->getType() << "(" << this->_shapes[i]->getName() << ")\n";
		}
		std::cin >> shapeChoice;
		if (this->_shapes.size() >= shapeChoice)
			flag = false;
	}
	flag = true;
	while (flag)
	{
		std::cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape.\n";
		std::cin >> action;
		switch (action)
		{
		case 0:
			std::cout << "Please enter the X moving scale: ";
			std::cin >> x;
			std::cout << "Please enter the Y moving scale: ";
			std::cin >> y;
			_shapes[shapeChoice]->clearDraw(_canvas);
			_shapes[shapeChoice]->move(Point(x, y));
			_shapes[shapeChoice]->draw(_canvas);
			flag = false;
			break;
		case 1:
			std::cout << _shapes[shapeChoice]->getName() << "      " << _shapes[shapeChoice]->getType() << "   " << _shapes[shapeChoice]->getArea() << "       " << _shapes[shapeChoice]->getPerimeter() << "\n";
			flag = false;
			break;
		case 2:
			_shapes[shapeChoice]->clearDraw(_canvas);
			_shapes.erase(_shapes.begin() + shapeChoice);
			flag = false;
			break;
		default:
			break;
		}
	}
}