#include "Triangle.h"

Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name) : Polygon(type,name)
{
	_points.push_back(a);
	_points.push_back(b);
	_points.push_back(c);
}

Triangle::~Triangle()
{
	_points.clear();
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

double Triangle::getArea() const
{
	double s = getPerimeter() / 2;
	double a = _points[0].distance(_points[1]);
	double b = _points[0].distance(_points[2]);
	double c = _points[1].distance(_points[2]);
	a = s - a;
	b = s - b;
	c = s - c;
	s = s * a * b * c;
	s = sqrt(s);
	return s;
}

double Triangle::getPerimeter() const
{
	return(_points[0].distance(_points[1]) + _points[0].distance(_points[2]) + _points[2].distance(_points[1]));
}

void Triangle::move(const Point& other)
{
	_points[0] += other;
	_points[1] += other;
	_points[2] += other;
}