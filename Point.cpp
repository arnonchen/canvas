#include "Point.h"
#include <math.h>
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}

Point::Point(const Point& other)
{
	_x = other.getX();
	_y = other.getY();
}

Point::~Point()
{
}

Point Point::operator+(const Point& other) const
{
	Point p(other.getX(), other.getY());
	return p;
}

Point& Point::operator+=(const Point& other)
{
	_x += other.getX();
	_y += other.getY();
	return *this;
}


double Point::getX() const
{
	return _x;
}

double Point::getY() const
{
	return _y;
}

double Point::distance(const Point& other) const
{
	double dis = 0;
	double x1 = _x;
	double x2 = other.getX();
	double y1 = _y;
	double y2 = other.getY();
	x2 = x2 - x1;
	y2 = y2 - y1;
	x2 *= x2;
	y2 *= y2;
	dis = x2 + y2;
	dis = sqrt(dis);
	return dis;
}